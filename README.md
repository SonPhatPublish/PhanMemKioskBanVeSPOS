﻿# Yeu cau cai dat

- Dam bao cai dat cac thanh phan sau:
    - vc++ all pack (https://www.techpowerup.com/download/visual-c-redistributable-runtime-package-all-in-one/) 2005 and up
    - .NET Framework 4.7
    - git (https://git-scm.com/)
    - driver xprinter (https://drive.google.com/file/d/1mlWDunnx7pIXr0hQDaiIMaEzoZtbpoC4/view)
    - cau hinh may in nhiet lam may in mac dinh
    - 7-zip (https://www.7-zip.org/) tuy chon

- Yeu cau cau hinh san cac van de sau:
    - Tat UAC: Vao Control Panel, tim kiem UAC, vao cai dat keo thanh cau hinh xuong thap nhat (Never Notify)
    - Bat tu dong dang nhap vao windows

# Huong dan cai dat

su dung cmd tren windows de chay lenh sau:

```sh
git clone https://gitlab.com/SonPhatPublish/PhanMemKioskBanVeSPOS.git
cd PhanMemKioskBanVeSPOS
explorer .
```

Sau khi cua so thu muc mo len, tim vaf mo ung dung spos.exe.
Ung dung se tu dong kich hoat auto startup, cac lan sau khi khoi dong lai may thi ung dung se tu dong mo.


# Nang cao

Khi can cau hinh nang cao, vui long lien he Trong de duoc huong dan chi tiet.